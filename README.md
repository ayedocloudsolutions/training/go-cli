# Go CLI

## Build a CLI in Golang

The goal of this project is to build a Command Line Interface (CLI) in [Golang](https://golang.org) that calculates a `BUILD_VERSION` from the Git Commit Sha (Short version), Git Branch and latest Git Tag of a given repository.

A `BUILD_VERSION` looks like this: `1.0.0-main.build-afb5d3cb`

It's composed of 3 values that can be obtained from a `git` repository:

- **Git Tag**: `1.0.0` (can be obtained like this: `git describe --abbrev=0 --tags`)
- **Git Branch**: `main` (can be obtained like this: `git rev-parse --abbrev-ref HEAD`)
- **Git Commit Sha** (short version): `afb5d3cb` (can be obtained like this: `git rev-parse --short HEAD`)
- the `build` keyword is static and must always be part of the `BUILD_VERSION`

## Requirements

- the CLI should be called `ship`
- the CLI should make use of:
  - [cobra CLI library](https://github.com/spf13/cobra)
  - [viper Config library](https://github.com/spf13/viper)
- the **Git Tag** must be a valid [Semantic Version](https://semver.org) number
- the `BUILD_VERSION` must be a valid [Semantic Version](https://semver.org) number
- if no **Git Tag** is available, it should default to `0.0.1`
- the `BUILD_VERSION` must not contain other characters than: `.-a-zA-Z0-9`
- the `get build-version` command should accept one or no argument - the $PATH to a git repository
- by default, the CLI should look for a Git repository in the current directory (`$PWD`)
- the CLI should fail with an error if
  - no valid Git repository can be found on the specified location
  - no valid Git Commit can be found on the specified location
  - no valid Git Tag can be found on the specified location
  - no valid Git Branch can be found on the specified location

## Outcome

The CLI should support the following commands:

- `ship get build-version $PATH`
  - returns the `BUILD_VERSION` of the repository at the given `$PATH`
- `ship get build-version`
  - returns the `BUILD_VERSION` to STDOUT: `1.0.0-main.build-afb5d3cb`
  - `$PATH` defaults to `$PWD`

## Get started

1. Clone/fork this repository
   1. there's already a tag associated with this repo: 1.0.0
2. Write your code to implement the CLI
3. Commit and push your changes to a private repo / fork
4. Generate a `BUILD_VERSION` for your latest commit to validate the CLI is working correctly
5. Add documentation to README.md outlining the following steps:
   1. How to **build** the application
   2. How to **run** the application
   3. How to **install** the application
